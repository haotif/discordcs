﻿using System.Diagnostics;
using DiscordRPC;

namespace DiscordCS
{
    public class DiscordRichPresence
    {
        private DiscordRpcClient _client;

        public DiscordRichPresence()
        {
            _client = new DiscordRpcClient(Settings.ClientId);

            _client.Initialize();

        }

        public void CheckProcess()
        {
            if (Process.GetProcessesByName("csgo").Length == 0)
            {
                _client.ClearPresence();
            }
        }

        public void SetRichPresence(RichPresenceData richPresenceData)
        {
            _client.SetPresence(new RichPresence
            {
                Details = richPresenceData.Details,
                State = richPresenceData.State,
                Assets = new Assets
                {
                    LargeImageKey = richPresenceData.LargeImage.Key,
                    LargeImageText = richPresenceData.LargeImage.Text,
                    SmallImageKey = richPresenceData.SmallImage.Key,
                    SmallImageText = richPresenceData.SmallImage.Text
                }
            });
        }
    }
}

