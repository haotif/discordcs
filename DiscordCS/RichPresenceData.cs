﻿namespace DiscordCS
{
	public class RichPresenceData
	{
		public RichPresenceData()
		{
			LargeImage = new Image();
			SmallImage = new Image();
		}
		
		public string Details { get; set; }
		public string State { get; set; }
		public Image LargeImage { get; set; }
		public Image SmallImage { get; set; }
	}

	public class Image
	{
		public string Key { get; set; }
		public string Text { get; set; }

		public Image()
		{
			
		}
		
		public Image(string key, string text)
		{
			Key = key;
			Text = text;
		}
	}
}