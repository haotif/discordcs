﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSGSI;
using CSGSI.Nodes;

namespace DiscordCS
{
	class Program
	{
		private static DiscordRichPresence _richPresence;
		private static RichPresenceData _richPresenceData = new RichPresenceData();
		public static async Task Main()
		{
			_richPresence = new DiscordRichPresence();

			var gameStateListener = new GameStateListener(3000);
			gameStateListener.Start();

			Console.WriteLine("Listener started successfully");

			gameStateListener.EnableRaisingIntricateEvents = true;
			gameStateListener.NewGameState += GameStateListenerOnNewGameState;

			while (true)
			{
				_richPresence.CheckProcess();
				await Task.Delay(5000);
			}
			
		}

		private static void GameStateListenerOnNewGameState(GameState gameState)
		{
			//If the user is on the menu
			if (gameState.Player.Activity == PlayerActivity.Menu)
			{
				//Clear everything apart from just title and large image
				_richPresenceData.Details = "In the menu...";
				_richPresenceData.State = "";
				_richPresenceData.LargeImage.Key = "menu";
				_richPresenceData.LargeImage.Text = "Menu";
				_richPresenceData.SmallImage.Key = "";
				_richPresenceData.SmallImage.Text = "";
			}
			//If the user is in game
			else if (gameState.Player.Activity == PlayerActivity.Playing)
			{
				//If the map state has changed
				if (gameState.Map != gameState.Previously.Map)
				{
					//Get an image object based on the map name
					var mapImage = ConvertMapName(gameState.Map.Name);
					
					_richPresenceData.LargeImage = mapImage;
					
					//If the player is on a team
					if (gameState.Player.Team != PlayerTeam.Undefined)
					{
						//If the player state has data, then set State to K/D, else set it to the map name
						_richPresenceData.State = gameState.Player.MatchStats.HasData
							? $"{gameState.Player.MatchStats.Kills}/{gameState.Player.MatchStats.Deaths} | {gameState.Player.State.Health}HP"
							: mapImage.Text;
						
						if (!Settings.SteamIds.Contains(gameState.Player.SteamID))
							_richPresenceData.State += " | Spectating";
					}

					//Set the Details to the gamemode
					_richPresenceData.Details = ConvertMapMode(gameState.Map.Mode);

					//If the gamemode is one which has rounds
					if (gameState.Map.Mode == MapMode.Casual || gameState.Map.Mode == MapMode.Competitive ||
					    gameState.Map.Mode == MapMode.ScrimComp2v2)
					{
						//Add the score to the details field
						switch (gameState.Player.Team)
						{
							case PlayerTeam.T:
								_richPresenceData.Details +=
									$" ({gameState.Map.TeamT.Score}-{gameState.Map.TeamCT.Score})";
								break;
							case PlayerTeam.CT:
								_richPresenceData.Details +=
									$" ({gameState.Map.TeamCT.Score}-{gameState.Map.TeamT.Score})";
								break;
						}
					}
				}

				//If the player state has changed
				if (gameState.Player.State != gameState.Previously.Player.State)
				{
					//If the player is in a team
					if (gameState.Player.Team != PlayerTeam.Undefined)
					{
						if (gameState.Player.Weapons.ActiveWeapon.HasData)
						{
							var activeWeapon = gameState.Player.Weapons.ActiveWeapon;

							if (activeWeapon.Type == WeaponType.C4 || activeWeapon.Type == WeaponType.Fists ||
							    activeWeapon.Type == WeaponType.Grenade || activeWeapon.Type == WeaponType.Knife ||
							    activeWeapon.Type == WeaponType.Melee || activeWeapon.Type == WeaponType.Tablet ||
							    activeWeapon.Type == WeaponType.BreachCharge)
							{
								if (activeWeapon.State == WeaponState.Active)
									_richPresenceData.SmallImage = new Image
									{
										Key	= activeWeapon.Name,
										Text = $"{ConvertWeaponName(activeWeapon.Name)}"
									};
							}
							else
							{
								if (activeWeapon.State == WeaponState.Active)
									_richPresenceData.SmallImage = new Image
									{
										Key	= activeWeapon.Name,
										Text = $"{ConvertWeaponName(activeWeapon.Name)} ({activeWeapon.AmmoClip}/{activeWeapon.AmmoReserve})"
									};
								else if (activeWeapon.State == WeaponState.Reloading)
								{
									_richPresenceData.SmallImage = new Image
									{
										Key = activeWeapon.Name,
										Text = $"{ConvertWeaponName(activeWeapon.Name)} | Reloading"
									};
									_richPresenceData.State += " | Reloading";
								}
							}
						}
						else
						{
							_richPresenceData.SmallImage = new Image
							{
								Key = "weapon_knife_ct",
								Text = $"{gameState.Player.State.Health}HP | {gameState.Player.State.Armor} Armour | ${gameState.Player.State.Money}"
							};
						}

						if (gameState.Player.State.Flashed > 0)
						{
							_richPresenceData.SmallImage = new Image
							{
								Key = "weapon_flashbang",
								Text = "Flashed"
							};
							_richPresenceData.State += " | Flashed";
						}
					}
				}
			}
			
			//Update the rich presence
			_richPresence.SetRichPresence(_richPresenceData);
		}

		private static string ConvertMapMode(MapMode mapMode)
		{
			return mapMode switch
			{
				MapMode.Undefined => "Undefined",
				MapMode.Casual => "Casual",
				MapMode.Competitive => "Competitive",
				MapMode.DeathMatch => "Deathmatch",
				MapMode.GunGameProgressive => "Arms Race",
				MapMode.GunGameTRBomb => "Demolition",
				MapMode.CoopMission => "Co-op Mission",
				MapMode.ScrimComp2v2 => "Wingman",
				MapMode.Custom => "Custom Game",
				MapMode.Survival => "Danger Zone",
				_ => "Unknown"
			};
		}

		private static string ConvertWeaponName(string weaponName)
		{
			return weaponName switch
			{
				"weapon_awp" => "AWP",
				"weapon_g3sg1" => "G3SG1",
				"weapon_scar20" => "SCAR-20",
				"weapon_ssg08" => "SSG 08",
				
				"weapon_ak47" => "AK-47",
				"weapon_aug" => "AUG",
				"weapon_famas" => "FAMAS",
				"weapon_galilar" => "Galil AR",
				"weapon_m4a1_silencer" => "M4A1-S",
				"weapon_m4a1" => "M4A4",
				"weapon_sg556" => "SG 553",
				
				"weapon_mac10" => "MAC-10",
				"weapon_mp5sd" => "MP5-SD",
				"weapon_mp7" => "MP7",
				"weapon_mp9" => "MP9",
				"weapon_p90" => "P90",
				"weapon_bizon" => "PP-Bizon",
				"weapon_ump45" => "UMP-45",
				
				"weapon_m249" => "M249",
				"weapon_negev" => "Negev",
				
				"weapon_mag7" => "MAG-7",
				"weapon_nova" => "Nova",
				"weapon_sawedoff" => "Sawed-Off",
				"weapon_xm1014" => "XM1014",
				
				"weapon_cz75a" => "CZ75-Auto",
				"weapon_deagle" => "Desert Eagle",
				"weapon_elite" => "Dual Berettas",
				"weapon_fiveseven" => "Five-SeveN",
				"weapon_glock" => "Glock-18",
				"weapon_hkp2000" => "P2000",
				"weapon_p250" => "P250",
				"weapon_revolver" => "R8 Revolver",
				"weapon_tec9" => "Tec-9",
				"weapon_usp_silencer" => "USP-S",
				
				"weapon_smoke" => "Smoke Grenade",
				"weapon_hegrenade" => "High Explosive Grenade",
				"weapon_molotov" => "Molotov",
				"weapon_incgrenade" => "Incendiary Grenade",
				"weapon_flashbang" => "Flashbang",
				
				"weapon_c4" => "C4",
				"weapon_knife_t" => "Knife",
				"weapon_knife_ct" => "Knife",
				
				_ => ""
			};
		}

		private static Image ConvertMapName(string mapName)
		{
			var knownMaps = new List<string>
			{
				"de_mirage", "de_inferno", "de_overpass", "de_vertigo", "de_nuke", "de_train", "de_dust2", "de_anubis",
				"de_cache", "cs_agency", "cs_office", "de_chlorine", "de_cbble", "de_canals", "cs_militia", "cs_italy",
				"cs_assualt", "gd_rialto", "de_shortdust", "de_lake", "ar_baggage", "de_safehouse", "de_stmarc",
				"ar_shoots", "ar_lunacy", "ar_monastery", "de_sugarcane", "de_bank"
			};

			if (knownMaps.All(m => m != mapName))
				return new Image
				{
					Key = "workshop",
					Text = "Workshop Map"
				};

			return mapName switch
			{
				"de_mirage" => new Image(mapName, "Mirage"),
				"de_inferno" => new Image(mapName, "Inferno"),
				"de_overpass" => new Image(mapName, "Overpass"),
				"de_vertigo" => new Image(mapName, "Vertigo"),
				"de_nuke" => new Image(mapName, "Nuke"),
				"de_train" => new Image(mapName, "Train"),
				"de_dust2" => new Image(mapName, "Dust II"),
				"de_anubis" => new Image(mapName, "Anubis"),
				"de_cache" => new Image(mapName, "Cache"),
				"cs_agency" => new Image(mapName, "Agency"),
				"cs_office" => new Image(mapName, "Office"),
				"de_chlorine" => new Image(mapName, "Chlorine"),
				"de_cbble" => new Image(mapName, "Cobblestone"),
				"de_canals" => new Image(mapName, "Canals"),
				"cs_militia" => new Image(mapName, "Militia"),
				"cs_italy" => new Image(mapName, "Italy"),
				"cs_assualt" => new Image(mapName, "Assault"),
				"gd_rialto" => new Image(mapName, "Rialto"),
				"de_shortdust" => new Image(mapName, "Shortdust"),
				"de_lake" => new Image(mapName, "Lake"),
				"ar_baggage" => new Image(mapName, "Baggage"),
				"de_safehouse" => new Image(mapName, "Safehouse"),
				"de_stmarc" => new Image(mapName, "St. Marc"),
				"ar_shoots" => new Image(mapName, "Shoots"),
				"ar_lunacy" => new Image(mapName, "Lunacy"),
				"ar_monastery" => new Image(mapName, "Monastery"),
				"de_sugarcane" => new Image(mapName, "Sugarcane"),
				"de_bank" => new Image(mapName, "Bank"),
				"ar_dizzy" => new Image(mapName, "Dizzy"),
				_ => new Image("workshop", "Workshop Map")
			};
		}
	}
}